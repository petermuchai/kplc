/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.equitybankgroup.prepaid.app.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author John Muli <john.muli at equitybank.co.ke>
 */
@Entity
@Table(name = "CUST_MASTER_TRAN_TABLE", schema="SHAREDATA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CustMasterTranTable.findAll", query = "SELECT c FROM CustMasterTranTable c"),
    @NamedQuery(name = "CustMasterTranTable.findByForacidAndStatus", query = "SELECT c FROM CustMasterTranTable c WHERE c.foracid = :foracid AND c.status = :status"),
    @NamedQuery(name = "CustMasterTranTable.findByAcctName", query = "SELECT c FROM CustMasterTranTable c WHERE c.acctName = :acctName"),
    @NamedQuery(name = "CustMasterTranTable.findByTranDate", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranDate = :tranDate"),
    @NamedQuery(name = "CustMasterTranTable.findByTranId", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranId = :tranId"),
    @NamedQuery(name = "CustMasterTranTable.findByPartTranSrlNum", query = "SELECT c FROM CustMasterTranTable c WHERE c.partTranSrlNum = :partTranSrlNum"),
    @NamedQuery(name = "CustMasterTranTable.findByDelFlg", query = "SELECT c FROM CustMasterTranTable c WHERE c.delFlg = :delFlg"),
    @NamedQuery(name = "CustMasterTranTable.findByTranType", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranType = :tranType"),
    @NamedQuery(name = "CustMasterTranTable.findByTranSubType", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranSubType = :tranSubType"),
    @NamedQuery(name = "CustMasterTranTable.findByPartTranType", query = "SELECT c FROM CustMasterTranTable c WHERE c.partTranType = :partTranType"),
    @NamedQuery(name = "CustMasterTranTable.findByAcid", query = "SELECT c FROM CustMasterTranTable c WHERE c.acid = :acid"),
    @NamedQuery(name = "CustMasterTranTable.findByValueDate", query = "SELECT c FROM CustMasterTranTable c WHERE c.valueDate = :valueDate"),
    @NamedQuery(name = "CustMasterTranTable.findByTranAmt", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranAmt = :tranAmt"),
    @NamedQuery(name = "CustMasterTranTable.findByTranParticular", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranParticular = :tranParticular"),
    @NamedQuery(name = "CustMasterTranTable.findByEntryUserId", query = "SELECT c FROM CustMasterTranTable c WHERE c.entryUserId = :entryUserId"),
    @NamedQuery(name = "CustMasterTranTable.findByPstdUserId", query = "SELECT c FROM CustMasterTranTable c WHERE c.pstdUserId = :pstdUserId"),
    @NamedQuery(name = "CustMasterTranTable.findByVfdUserId", query = "SELECT c FROM CustMasterTranTable c WHERE c.vfdUserId = :vfdUserId"),
    @NamedQuery(name = "CustMasterTranTable.findByEntryDate", query = "SELECT c FROM CustMasterTranTable c WHERE c.entryDate = :entryDate"),
    @NamedQuery(name = "CustMasterTranTable.findByPstdDate", query = "SELECT c FROM CustMasterTranTable c WHERE c.pstdDate = :pstdDate"),
    @NamedQuery(name = "CustMasterTranTable.findByVfdDate", query = "SELECT c FROM CustMasterTranTable c WHERE c.vfdDate = :vfdDate"),
    @NamedQuery(name = "CustMasterTranTable.findByRefNum", query = "SELECT c FROM CustMasterTranTable c WHERE c.refNum = :refNum"),
    @NamedQuery(name = "CustMasterTranTable.findByInstrmntDate", query = "SELECT c FROM CustMasterTranTable c WHERE c.instrmntDate = :instrmntDate"),
    @NamedQuery(name = "CustMasterTranTable.findByInstrmntNum", query = "SELECT c FROM CustMasterTranTable c WHERE c.instrmntNum = :instrmntNum"),
    @NamedQuery(name = "CustMasterTranTable.findByInstrmntAlpha", query = "SELECT c FROM CustMasterTranTable c WHERE c.instrmntAlpha = :instrmntAlpha"),
    @NamedQuery(name = "CustMasterTranTable.findByTranRmks", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranRmks = :tranRmks"),
    @NamedQuery(name = "CustMasterTranTable.findByPstdFlg", query = "SELECT c FROM CustMasterTranTable c WHERE c.pstdFlg = :pstdFlg"),
    @NamedQuery(name = "CustMasterTranTable.findByRcreUserId", query = "SELECT c FROM CustMasterTranTable c WHERE c.rcreUserId = :rcreUserId"),
    @NamedQuery(name = "CustMasterTranTable.findByRcreTime", query = "SELECT c FROM CustMasterTranTable c WHERE c.rcreTime = :rcreTime"),
    @NamedQuery(name = "CustMasterTranTable.findByCustId", query = "SELECT c FROM CustMasterTranTable c WHERE c.custId = :custId"),
    @NamedQuery(name = "CustMasterTranTable.findByDeliveryChannelId", query = "SELECT c FROM CustMasterTranTable c WHERE c.deliveryChannelId = :deliveryChannelId"),
    @NamedQuery(name = "CustMasterTranTable.findByInitSolId", query = "SELECT c FROM CustMasterTranTable c WHERE c.initSolId = :initSolId"),
    @NamedQuery(name = "CustMasterTranTable.findByStatus", query = "SELECT c FROM CustMasterTranTable c WHERE c.status = :status"),
    @NamedQuery(name = "CustMasterTranTable.findByRemarks", query = "SELECT c FROM CustMasterTranTable c WHERE c.remarks = :remarks"),
    @NamedQuery(name = "CustMasterTranTable.findByType", query = "SELECT c FROM CustMasterTranTable c WHERE c.type = :type"),
    @NamedQuery(name = "CustMasterTranTable.findByTranCrncyCode", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranCrncyCode = :tranCrncyCode"),
    @NamedQuery(name = "CustMasterTranTable.findByTranClientType", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranClientType = :tranClientType"),
    @NamedQuery(name = "CustMasterTranTable.findByTranTypeMemo", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranTypeMemo = :tranTypeMemo"),
    @NamedQuery(name = "CustMasterTranTable.findByTranAgnAccNo", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranAgnAccNo = :tranAgnAccNo"),
    @NamedQuery(name = "CustMasterTranTable.findByTranPaidBy", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranPaidBy = :tranPaidBy"),
    @NamedQuery(name = "CustMasterTranTable.findByTranBrnShtDesc", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranBrnShtDesc = :tranBrnShtDesc"),
    @NamedQuery(name = "CustMasterTranTable.findByTranRctType", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranRctType = :tranRctType"),
    @NamedQuery(name = "CustMasterTranTable.findByTranSystem", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranSystem = :tranSystem"),
    @NamedQuery(name = "CustMasterTranTable.findByTranFmsRctNo", query = "SELECT c FROM CustMasterTranTable c WHERE c.tranFmsRctNo = :tranFmsRctNo"),
    @NamedQuery(name = "CustMasterTranTable.findById", query = "SELECT c FROM CustMasterTranTable c WHERE c.id = :id"),
    @NamedQuery(name = "CustMasterTranTable.findByCountryCode", query = "SELECT c FROM CustMasterTranTable c WHERE c.countryCode = :countryCode"),
    @NamedQuery(name = "CustMasterTranTable.findByBankCode", query = "SELECT c FROM CustMasterTranTable c WHERE c.bankCode = :bankCode"),
    @NamedQuery(name = "CustMasterTranTable.findByBrCode", query = "SELECT c FROM CustMasterTranTable c WHERE c.brCode = :brCode"),
    @NamedQuery(name = "CustMasterTranTable.findByPayingAcctId", query = "SELECT c FROM CustMasterTranTable c WHERE c.payingAcctId = :payingAcctId"),
    @NamedQuery(name = "CustMasterTranTable.findByRecTimestamp", query = "SELECT c FROM CustMasterTranTable c WHERE c.recTimestamp = :recTimestamp")})
public class CustMasterTranTable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "FORACID")
    private String foracid;
    @Column(name = "ACCT_NAME")
    private String acctName;
    @Column(name = "TRAN_DATE")
    @Temporal(TemporalType.DATE)
    private Date tranDate;
    @Column(name = "TRAN_ID")
    private String tranId;
    @Column(name = "PART_TRAN_SRL_NUM")
    private String partTranSrlNum;
    @Column(name = "DEL_FLG")
    private Character delFlg;
    @Column(name = "TRAN_TYPE")
    private Character tranType;
    @Column(name = "TRAN_SUB_TYPE")
    private String tranSubType;
    @Column(name = "PART_TRAN_TYPE")
    private Character partTranType;
    @Column(name = "ACID")
    private String acid;
    @Column(name = "VALUE_DATE")
    @Temporal(TemporalType.DATE)
    private Date valueDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TRAN_AMT")
    private BigDecimal tranAmt;
    @Column(name = "TRAN_PARTICULAR")
    private String tranParticular;
    @Column(name = "ENTRY_USER_ID")
    private String entryUserId;
    @Column(name = "PSTD_USER_ID")
    private String pstdUserId;
    @Column(name = "VFD_USER_ID")
    private String vfdUserId;
    @Column(name = "ENTRY_DATE")
    @Temporal(TemporalType.DATE)
    private Date entryDate;
    @Column(name = "PSTD_DATE")
    @Temporal(TemporalType.DATE)
    private Date pstdDate;
    @Column(name = "VFD_DATE")
    @Temporal(TemporalType.DATE)
    private Date vfdDate;
    @Column(name = "REF_NUM")
    private String refNum;
    @Column(name = "INSTRMNT_DATE")
    @Temporal(TemporalType.DATE)
    private Date instrmntDate;
    @Column(name = "INSTRMNT_NUM")
    private String instrmntNum;
    @Column(name = "INSTRMNT_ALPHA")
    private String instrmntAlpha;
    @Column(name = "TRAN_RMKS")
    private String tranRmks;
    @Column(name = "PSTD_FLG")
    private Character pstdFlg;
    @Column(name = "RCRE_USER_ID")
    private String rcreUserId;
    @Column(name = "RCRE_TIME")
    @Temporal(TemporalType.DATE)
    private Date rcreTime;
    @Column(name = "CUST_ID")
    private String custId;
    @Column(name = "DELIVERY_CHANNEL_ID")
    private String deliveryChannelId;
    @Column(name = "INIT_SOL_ID")
    private String initSolId;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "REMARKS")
    private String remarks;
    @Column(name = "TYPE")
    private String type;
    @Column(name = "TRAN_CRNCY_CODE")
    private String tranCrncyCode;
    @Column(name = "TRAN_CLIENT_TYPE")
    private String tranClientType;
    @Column(name = "TRAN_TYPE_MEMO")
    private String tranTypeMemo;
    @Column(name = "TRAN_AGN_ACC_NO")
    private String tranAgnAccNo;
    @Column(name = "TRAN_PAID_BY")
    private String tranPaidBy;
    @Column(name = "TRAN_BRN_SHT_DESC")
    private String tranBrnShtDesc;
    @Column(name = "TRAN_RCT_TYPE")
    private String tranRctType;
    @Column(name = "TRAN_SYSTEM")
    private String tranSystem;
    @Column(name = "TRAN_FMS_RCT_NO")
    private BigInteger tranFmsRctNo;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "COUNTRY_CODE")
    private String countryCode;
    @Column(name = "BANK_CODE")
    private String bankCode;
    @Column(name = "BR_CODE")
    private String brCode;
    @Column(name = "PAYING_ACCT_ID")
    private String payingAcctId;
    @Column(name = "REC_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date recTimestamp;

    public CustMasterTranTable() {
    }

    public CustMasterTranTable(Long id) {
        this.id = id;
    }

    public CustMasterTranTable(Long id, String countryCode) {
        this.id = id;
        this.countryCode = countryCode;
    }

    public String getForacid() {
        return foracid;
    }

    public void setForacid(String foracid) {
        this.foracid = foracid;
    }

    public String getAcctName() {
        return acctName;
    }

    public void setAcctName(String acctName) {
        this.acctName = acctName;
    }

    public Date getTranDate() {
        return tranDate;
    }

    public void setTranDate(Date tranDate) {
        this.tranDate = tranDate;
    }

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getPartTranSrlNum() {
        return partTranSrlNum;
    }

    public void setPartTranSrlNum(String partTranSrlNum) {
        this.partTranSrlNum = partTranSrlNum;
    }

    public Character getDelFlg() {
        return delFlg;
    }

    public void setDelFlg(Character delFlg) {
        this.delFlg = delFlg;
    }

    public Character getTranType() {
        return tranType;
    }

    public void setTranType(Character tranType) {
        this.tranType = tranType;
    }

    public String getTranSubType() {
        return tranSubType;
    }

    public void setTranSubType(String tranSubType) {
        this.tranSubType = tranSubType;
    }

    public Character getPartTranType() {
        return partTranType;
    }

    public void setPartTranType(Character partTranType) {
        this.partTranType = partTranType;
    }

    public String getAcid() {
        return acid;
    }

    public void setAcid(String acid) {
        this.acid = acid;
    }

    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    public BigDecimal getTranAmt() {
        return tranAmt;
    }

    public void setTranAmt(BigDecimal tranAmt) {
        this.tranAmt = tranAmt;
    }

    public String getTranParticular() {
        return tranParticular;
    }

    public void setTranParticular(String tranParticular) {
        this.tranParticular = tranParticular;
    }

    public String getEntryUserId() {
        return entryUserId;
    }

    public void setEntryUserId(String entryUserId) {
        this.entryUserId = entryUserId;
    }

    public String getPstdUserId() {
        return pstdUserId;
    }

    public void setPstdUserId(String pstdUserId) {
        this.pstdUserId = pstdUserId;
    }

    public String getVfdUserId() {
        return vfdUserId;
    }

    public void setVfdUserId(String vfdUserId) {
        this.vfdUserId = vfdUserId;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public Date getPstdDate() {
        return pstdDate;
    }

    public void setPstdDate(Date pstdDate) {
        this.pstdDate = pstdDate;
    }

    public Date getVfdDate() {
        return vfdDate;
    }

    public void setVfdDate(Date vfdDate) {
        this.vfdDate = vfdDate;
    }

    public String getRefNum() {
        return refNum;
    }

    public void setRefNum(String refNum) {
        this.refNum = refNum;
    }

    public Date getInstrmntDate() {
        return instrmntDate;
    }

    public void setInstrmntDate(Date instrmntDate) {
        this.instrmntDate = instrmntDate;
    }

    public String getInstrmntNum() {
        return instrmntNum;
    }

    public void setInstrmntNum(String instrmntNum) {
        this.instrmntNum = instrmntNum;
    }

    public String getInstrmntAlpha() {
        return instrmntAlpha;
    }

    public void setInstrmntAlpha(String instrmntAlpha) {
        this.instrmntAlpha = instrmntAlpha;
    }

    public String getTranRmks() {
        return tranRmks;
    }

    public void setTranRmks(String tranRmks) {
        this.tranRmks = tranRmks;
    }

    public Character getPstdFlg() {
        return pstdFlg;
    }

    public void setPstdFlg(Character pstdFlg) {
        this.pstdFlg = pstdFlg;
    }

    public String getRcreUserId() {
        return rcreUserId;
    }

    public void setRcreUserId(String rcreUserId) {
        this.rcreUserId = rcreUserId;
    }

    public Date getRcreTime() {
        return rcreTime;
    }

    public void setRcreTime(Date rcreTime) {
        this.rcreTime = rcreTime;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getDeliveryChannelId() {
        return deliveryChannelId;
    }

    public void setDeliveryChannelId(String deliveryChannelId) {
        this.deliveryChannelId = deliveryChannelId;
    }

    public String getInitSolId() {
        return initSolId;
    }

    public void setInitSolId(String initSolId) {
        this.initSolId = initSolId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTranCrncyCode() {
        return tranCrncyCode;
    }

    public void setTranCrncyCode(String tranCrncyCode) {
        this.tranCrncyCode = tranCrncyCode;
    }

    public String getTranClientType() {
        return tranClientType;
    }

    public void setTranClientType(String tranClientType) {
        this.tranClientType = tranClientType;
    }

    public String getTranTypeMemo() {
        return tranTypeMemo;
    }

    public void setTranTypeMemo(String tranTypeMemo) {
        this.tranTypeMemo = tranTypeMemo;
    }

    public String getTranAgnAccNo() {
        return tranAgnAccNo;
    }

    public void setTranAgnAccNo(String tranAgnAccNo) {
        this.tranAgnAccNo = tranAgnAccNo;
    }

    public String getTranPaidBy() {
        return tranPaidBy;
    }

    public void setTranPaidBy(String tranPaidBy) {
        this.tranPaidBy = tranPaidBy;
    }

    public String getTranBrnShtDesc() {
        return tranBrnShtDesc;
    }

    public void setTranBrnShtDesc(String tranBrnShtDesc) {
        this.tranBrnShtDesc = tranBrnShtDesc;
    }

    public String getTranRctType() {
        return tranRctType;
    }

    public void setTranRctType(String tranRctType) {
        this.tranRctType = tranRctType;
    }

    public String getTranSystem() {
        return tranSystem;
    }

    public void setTranSystem(String tranSystem) {
        this.tranSystem = tranSystem;
    }

    public BigInteger getTranFmsRctNo() {
        return tranFmsRctNo;
    }

    public void setTranFmsRctNo(BigInteger tranFmsRctNo) {
        this.tranFmsRctNo = tranFmsRctNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBrCode() {
        return brCode;
    }

    public void setBrCode(String brCode) {
        this.brCode = brCode;
    }

    public String getPayingAcctId() {
        return payingAcctId;
    }

    public void setPayingAcctId(String payingAcctId) {
        this.payingAcctId = payingAcctId;
    }

    public Date getRecTimestamp() {
        return recTimestamp;
    }

    public void setRecTimestamp(Date recTimestamp) {
        this.recTimestamp = recTimestamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustMasterTranTable)) {
            return false;
        }
        CustMasterTranTable other = (CustMasterTranTable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.equitybank.kplcprepaid.entity.CustMasterTranTable[ id=" + id + " ]";
    }
    
}
