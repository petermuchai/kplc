package com.equitybankgroup.prepaid.app.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "FAILEDTOKENREQUESTS")
public class FailedRequests implements Serializable  {
	
	 private static final long serialVersionUID = 1L;
	 
	 @Id
	 @GeneratedValue(strategy = GenerationType.AUTO)
	 private Long id;
	 
	 @Column(name="FORACID")
	 private String foracid;
	 @Column(name="TRAN_DATE")
	 @Temporal(javax.persistence.TemporalType.TIMESTAMP)
	 private Date trandate;
	 @Column(name="TRAN_ID")
	 private String tranId;
	 @Column(name="STATUS")
	 private String status;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getForacid() {
		return foracid;
	}
	public void setForacid(String foracid) {
		this.foracid = foracid;
	}
	public Date getTrandate() {
		return trandate;
	}
	public void setTrandate(Date trandate) {
		this.trandate = trandate;
	}
	public String getTranId() {
		return tranId;
	}
	public void setTranId(String tranId) {
		this.tranId = tranId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	@Override
	public String toString() {
		return "FailedRequests [id=" + id + ", foracid=" + foracid
				+ ", trandate=" + trandate + ", tranId=" + tranId + ", status="
				+ status + "]";
	}
	
	
	 
	 
	 

}
