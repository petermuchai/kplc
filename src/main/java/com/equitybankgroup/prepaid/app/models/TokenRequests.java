/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equitybankgroup.prepaid.app.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author John Muli <john.muli at equitybank.co.ke>
 */
@Entity
@Table(name = "TOKENREQUESTS")
@NamedQueries({
    @NamedQuery(name = "TokenRequests.findAll", query = "SELECT r FROM TokenRequests r")
})
public class TokenRequests implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "DATAID")
    private String dataid;
    @Column(name = "AMOUNT")
    private BigDecimal amount;
    @Column(name = "CURRENCY")
    private String currency;
    @Column(name = "METERNUMBER")
    private String meterno;
    @Column(name = "TOKEN")
    private String token;
    @Column(name = "TRANSACTIONREF")
    private String transactionRef;
    @Column(name = "RESPONSECODE")
    private String responseCode;
    @Column(name = "UNITS")
    private BigDecimal units;
    @Column(name = "TOKENDETAILS")
    private String tokenDetails;
    @Column(name = "CUSTOMERDETAILS")
    private String customerDetails;
    @Column(name = "CUSTOMERMESSAGE")
    private String customerMessage;
    @Column(name = "DATEREQUESTED")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateRequested;
    @Column(name = "DATERECEIVED")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateReceived;
    @Column(name = "STATUS")
    private Integer status;
    @Column(name = "RETRIES")
    private Integer retries;
    @Column(name = "PHONENO")
    private String phoneNo;

    public Integer getRetries() {
        return retries;
    }

    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTransactionRef() {
        return transactionRef;
    }

    public void setTransactionRef(String transactionRef) {
        this.transactionRef = transactionRef;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMeterno() {
        return meterno;
    }

    public void setMeterno(String meterno) {
        this.meterno = meterno;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataid() {
        return dataid;
    }

    public void setDataid(String dataid) {
        this.dataid = dataid;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public BigDecimal getUnits() {
        return units;
    }

    public void setUnits(BigDecimal units) {
        this.units = units;
    }

    public String getTokenDetails() {
        return tokenDetails;
    }

    public void setTokenDetails(String tokenDetails) {
        this.tokenDetails = tokenDetails;
    }

    public String getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(String customerDetails) {
        this.customerDetails = customerDetails;
    }

    public String getCustomerMessage() {
        return customerMessage;
    }

    public void setCustomerMessage(String customerMessage) {
        this.customerMessage = customerMessage;
    }

    public Date getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(Date dateRequested) {
        this.dateRequested = dateRequested;
    }

    public Date getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(Date dateReceived) {
        this.dateReceived = dateReceived;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TokenRequests)) {
            return false;
        }
        TokenRequests other = (TokenRequests) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.equitybank.kplcprepaid.entity.TokenRequests[ id=" + id + " ]";
    }

}
