package com.equitybankgroup.prepaid.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.equitybankgroup.prepaid.app.services.ReversalManager;

@RestController
@RequestMapping("/tokenreversals")
public class TokenReversalController {
	@Autowired
	ReversalManager reversalManager;

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView getTokenReversals(Pageable pageable) {
		ModelAndView mv = new ModelAndView("tokenreversals");
		mv.addObject("tokenreversals", reversalManager.getReversal(pageable));
		return mv;
	}

	@RequestMapping(value = "/search.do", method = RequestMethod.GET)
	public ModelAndView loadSearchForm() {
		ModelAndView mv = new ModelAndView("searchtokenreversals");
		return mv;
	}

	@RequestMapping(value = "/search/{meterNumber}/{startDate}/{endDate}")
	public ModelAndView searchTokenRequests(@PathVariable String meterNumber, @PathVariable String startDate,
			@PathVariable String endDate, Pageable pageable) {
		ModelAndView mv = new ModelAndView("searchtokenreversals");
		mv.addObject("tokenreversals",
				reversalManager.getSearchedTokenReversals(meterNumber, startDate, endDate, pageable));
		return mv;
	}

}
