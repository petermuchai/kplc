package com.equitybankgroup.prepaid.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.equitybankgroup.prepaid.app.services.RequestManager;

@Controller
public class DefaultController {
	@Autowired
	RequestManager requestManager;

	@RequestMapping("/")
	public String index() {
		return "index";
	}

	/*@RequestMapping(value= "/export", method = RequestMethod.GET)
	public ModelAndView exportExcel() {
		ModelAndView mv = new ModelAndView("excelView","requests",requestManager.getTokenRequest());
		return mv;
	}*/
}
