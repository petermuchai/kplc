package com.equitybankgroup.prepaid.app.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.equitybankgroup.prepaid.app.models.FailedRequests;
import com.equitybankgroup.prepaid.app.repositories.FailedRequestsRepository;
import com.equitybankgroup.prepaid.app.services.FailedReqManager;

@RestController
@RequestMapping("/failedRQs")
public class FailedRequestController {

	@Autowired
	FailedReqManager failedReqManager;
	@Autowired
	FailedRequestsRepository failedRequestsRepository;
	
	/*@ModelAttribute("failedRequests")
	@RequestMapping(method = RequestMethod.GET)
	public List<FailedRequests> getFailedTokenRequests() {
		return (List<FailedRequests>) failedRequestsRepository.findAll();
	}*/
	
	@RequestMapping(method = RequestMethod.GET)
	public Page<FailedRequests> getFailedTokenRequests(Pageable pageable) {
		return failedReqManager.getFailedRequets(pageable);
	}

	@RequestMapping(value = "/{filter}", method = RequestMethod.GET)
	public Page<FailedRequests> getFailedReqsByForacid(
			@PathVariable("filter") String filter, Pageable pageable) {
		return failedReqManager.getFailedReqByForacid(filter, pageable);
	}

	@RequestMapping(value = "/{filter}/reset", method = RequestMethod.GET)
	public void resetFailedRequests(@PathVariable("filter") String filter,
			HttpServletResponse response) throws IOException {
		List<FailedRequests> failedRequests = failedReqManager
				.getFailedRequestByForacid(filter);
		boolean state = failedReqManager.resetRequestStatus(failedRequests);
		if (state) {
			response.getWriter().print(
					"{success:true, msg:'Reset Successful!'}");
		} else {
			response.getWriter().print(" {success:false, msg:'Reset Failed!'}");
		}
	}
	
	@RequestMapping(value="/id/{filter}", method = RequestMethod.GET)
	public FailedRequests getFailedRequestByID(@PathVariable("filter") Long id){
		return failedReqManager.getFailedRequestById(id);
	}

}
