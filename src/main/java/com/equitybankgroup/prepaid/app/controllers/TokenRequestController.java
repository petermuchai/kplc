package com.equitybankgroup.prepaid.app.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.util.StringUtils;

import com.equitybankgroup.prepaid.app.models.TokenRequests;
import com.equitybankgroup.prepaid.app.services.RequestExcelView;
import com.equitybankgroup.prepaid.app.services.RequestManager;
import com.equitybankgroup.prepaid.app.utils.PageWrapper;

@RestController
@RequestMapping("/tokenrequests")
public class TokenRequestController {
	@Autowired
	RequestManager requestManager;

	// PageableArgumentResolver --- automatically resolves request parameters to
	// build a PageRequest instance
	@RequestMapping(method = RequestMethod.GET)
	public Page<TokenRequests> getTokenRequestsCount(Pageable pageable) {
		Page<TokenRequests> pageResult = requestManager.gettokenRequestCount(pageable);
		return pageResult;
	}

	@RequestMapping(value = "/tokenrequests2", method = RequestMethod.GET)
	public ModelAndView getTokenRequest(Pageable pageable) {
		ModelAndView uiModel = new ModelAndView("tokenrequests2");
		PageWrapper<TokenRequests> pageResult = new PageWrapper<TokenRequests>(requestManager.getTokenRequest(pageable),
				"/tokenrequests/tokenrequests2");
		uiModel.addObject("page", pageResult);
		return uiModel;
	}

	@RequestMapping(value = "/tokenrequests22", method = RequestMethod.GET)
	public PageWrapper<TokenRequests> getTokenRequestt(Model uiModel, Pageable pageable) {
		PageWrapper<TokenRequests> pageResult = new PageWrapper<TokenRequests>(requestManager.getTokenRequest(pageable),
				"/tokenrequests/tokenrequests2");
		uiModel.addAttribute("page", pageResult);
		return pageResult;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView requests(Pageable pageable, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("tokenrequests");
		PageWrapper<TokenRequests> pageResult = new PageWrapper<TokenRequests>(requestManager.getTokenRequest(pageable),
				"/tokenrequests/show");
		mav.addObject("page", pageResult);
		return mav;
	}

	// load search form
	@RequestMapping(value = "/search.do", method = RequestMethod.GET)
	public ModelAndView loadSearchForm() {
		ModelAndView mv = new ModelAndView("searchtokenrequests");
		return mv;
	}

	@RequestMapping(value = "/search/{phoneNo}/{startDate}/{endDate}")
	public ModelAndView searchTokenRequests(@PathVariable String phoneNo, @PathVariable String startDate,
			@PathVariable String endDate, Pageable pageable, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("searchtokenrequests");
		PageWrapper<TokenRequests> pageResult = new PageWrapper<TokenRequests>(
				requestManager.getSearchedTokenRequests(phoneNo, startDate, endDate, pageable),
				request.getRequestURI().toString());
		mv.addObject("page", pageResult);
		return mv;
	}

	@RequestMapping(value = "/search/{startDate}/{endDate}")
	public ModelAndView searchTokenRequestsByDate(@PathVariable String startDate, @PathVariable String endDate,
			Pageable pageable, HttpServletRequest request) {
		String phoneNo = null;
		ModelAndView mv = new ModelAndView("searchtokenrequests");
		PageWrapper<TokenRequests> pageResult = new PageWrapper<TokenRequests>(
				requestManager.getSearchedTokenRequests(phoneNo, startDate, endDate, pageable),
				request.getRequestURI().toString());
		mv.addObject("page", pageResult);
		return mv;
	}

	@RequestMapping(value = "/search/{id}", method = RequestMethod.GET)
	public ModelAndView getFilteredTokenRequests(@PathVariable Long id) {
		ModelAndView mv = new ModelAndView("singletokenrequest");
		mv.addObject("tokenrequests", requestManager.getTokenRequestById(id));
		return mv;
	}

	// export transactions to excel
	@RequestMapping(value = "/Transactions", method = RequestMethod.GET)
	public ModelAndView exportAllTokenRqs() {
		return new ModelAndView(new RequestExcelView(), "Transactions", requestManager.getTokenRequestList());
	}

	// export transactions to excel
	@RequestMapping(value = "/TransactionsByDate/{startDate}/{endDate}", method = RequestMethod.GET)
	public ModelAndView exportSearchedTokenRqs(@PathVariable String startDate, @PathVariable String endDate,
			HttpServletRequest request) {
		String phoneNo = null;
		List<TokenRequests> resultList = new ArrayList<TokenRequests>();
		List<TokenRequests> tokenRequestList = (List<TokenRequests>) requestManager.searchedTokenRqExcelList(phoneNo,
				startDate, endDate);
		for (TokenRequests tokenRequest : tokenRequestList) {
			if (tokenRequest.getResponseCode() != null) {
				if (tokenRequest.getResponseCode().equals("elec000")) {
					tokenRequest.setResponseCode("Success");
				} else {
					tokenRequest.setResponseCode("Failed");
				}
			} else {
				tokenRequest.setResponseCode("Failed");
			}
			tokenRequest.setTransactionRef(StringUtils.substring(tokenRequest.getTransactionRef(), 8,
					StringUtils.length(tokenRequest.getTransactionRef()) - 1));
			resultList.add(tokenRequest);
		}
		return new ModelAndView(new RequestExcelView(), "Transactions", resultList);
	}

}
