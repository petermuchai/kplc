package com.equitybankgroup.prepaid.app.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by john on 6/4/2015.
 */
public class RequestObj {
    private Integer page;
    private Integer size;
    private String filter;
    private Date dateStart;
    private Date dateEnd;
    private String phoneNo;

    public RequestObj(){

        Calendar cal = Calendar.getInstance();
        if(dateStart==null){
            cal.add(Calendar.DATE,-1);
            dateStart = cal.getTime();
        }
        if(dateEnd==null){


            dateEnd = new Date();
        }

        if(page==null){
            page=0;
        }
        if(size==null){
            size=200;
        }
        if(filter==null){
            filter="";
        }

    }


    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getPhoneNo() {
		return phoneNo;
	}


	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}


	@Override
    public String toString() {
        return "RequestObj{" +
                "page=" + page +
                ", size=" + size +
                ", filter='" + filter + '\'' +
                ", dateStart=" + dateStart +
                ", dateEnd=" + dateEnd +
                '}';
    }
}
