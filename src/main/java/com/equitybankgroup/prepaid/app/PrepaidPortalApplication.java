package com.equitybankgroup.prepaid.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@EnableAutoConfiguration
@Configuration
@ComponentScan("com.equitybankgroup.prepaid.app")
public class PrepaidPortalApplication {

	public static void main(String[] args) {

		SpringApplication.run(PrepaidPortalApplication.class, args);
	}

}
