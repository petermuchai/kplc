package com.equitybankgroup.prepaid.app.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import com.equitybankgroup.prepaid.app.models.TokenRequests;
import com.equitybankgroup.prepaid.app.repositories.RequestRepository;

/**
 * Created by john on 6/4/2015.
 */
@Service
public class RequestManager {

	@Autowired
	RequestRepository requestRepository;

	/*public List<TokenRequests> getTokenRequestList() {
		return (List<TokenRequests>) requestRepository.findAll();
	}*/

	public Page<TokenRequests> getTokenRequest(Pageable pageable) {
		System.out.println("Invoked");
		return (Page<TokenRequests>) requestRepository.findAllRequests(pageable);
	}

	public List<TokenRequests> getTokenRequestList() {
		List<TokenRequests> resultList = new ArrayList<TokenRequests>();
		List<TokenRequests> tokenRequestList = (List<TokenRequests>) requestRepository
				.findAll(new Sort(Sort.Direction.DESC, "dateReceived"));
		for (TokenRequests tokenRequest : tokenRequestList) {
			if (tokenRequest.getResponseCode() != null) {
				if (tokenRequest.getResponseCode().equals("elec000")) {
					tokenRequest.setResponseCode("Success");
				} else {
					tokenRequest.setResponseCode("Failed");
				}
			} else {
				tokenRequest.setResponseCode("Failed");
			}
			tokenRequest.setTransactionRef(StringUtils.substring(tokenRequest.getTransactionRef(), 8,
					StringUtils.length(tokenRequest.getTransactionRef()) - 1));
			resultList.add(tokenRequest);
		}
		return resultList;
	}

	public Page<TokenRequests> getTokenFiltered(String filter, PageRequest req) {
		return requestRepository.findByMeternoContaining(filter, req);
	}

	public Page<TokenRequests> getFilteredToken(String filter, Pageable pageable) {
		return requestRepository.findByMeternoContaining(filter, pageable);
	}

	public Page<TokenRequests> getSearchedTokenRequests(String phoneNo, String startDate, String endDate,
			Pageable pageable) {
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
		try {
			startDate = formatter2.format(formatter2.parse(formatter2.format(formatter1.parse(startDate))));
			endDate = formatter2.format(formatter2.parse(formatter2.format(formatter1.parse(endDate))));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (phoneNo != null) {
			return requestRepository.searchTokenRequests(phoneNo, startDate, endDate, pageable);
		} else {
			return requestRepository.searchTokenRequestsByDate(startDate, endDate, pageable);
		}
	}

	public TokenRequests getTokenRequestById(Long id) {
		TokenRequests tokenRequest = requestRepository.findOne(id);
		if (tokenRequest.getResponseCode() != null) {
			if (tokenRequest.getResponseCode().equals("elec000")) {
				System.out.println(tokenRequest.getResponseCode());
			} else {
				tokenRequest.setResponseCode("Failed");
			}
		}
		return tokenRequest;
	}

	public Page<TokenRequests> gettokenRequestCount(Pageable pageable) {
		return requestRepository.tokenRequestCount(pageable);
	}

	public TokenRequests getTokenByRef(String ref) {
		List<TokenRequests> reqs = requestRepository.findByTransactionRef(ref,
				new Sort(Sort.Direction.DESC, "dateReceived"));

		if (reqs.size() > 0) {
			return reqs.get(0);
		} else {
			return null;
		}
	}
	
	public List<TokenRequests> searchedTokenRqExcelList(String phoneNo, String startDate, String endDate){
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
		try {
			startDate = formatter2.format(formatter2.parse(formatter2.format(formatter1.parse(startDate))));
			endDate = formatter2.format(formatter2.parse(formatter2.format(formatter1.parse(endDate))));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return requestRepository.searchTokenRequestsByDateExcel(startDate, endDate);
	}
}
