package com.equitybankgroup.prepaid.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.equitybankgroup.prepaid.app.models.CustMasterTranTable;
import com.equitybankgroup.prepaid.app.repositories.TransactionRepository;

/**
 * Created by john on 6/4/2015.
 */
@Service
public class TxManager {

    @Autowired
    public TransactionRepository repository;

    public Page<CustMasterTranTable> getTransactions(PageRequest req) {
        return repository.findAll(req);
    }

    public Page<CustMasterTranTable> getTransactions(String foracid, PageRequest req) {
        return repository.findByForacid(foracid, req);
    }

    public Page<CustMasterTranTable> getFailedTransactions(PageRequest req) {
        return repository.findFailures(req);
    }

    public Page<CustMasterTranTable> getTxFiltered(String filter, PageRequest req) {
        return repository.findByRefNumContaining(filter, req);
    }

    public CustMasterTranTable getTransaction(Long transactionID) {
        return repository.findOne(transactionID);
    }
    
    public boolean updateCustomerData(CustMasterTranTable custMasterTranTable){
    	if(repository.save(custMasterTranTable) != null){
    		return true;
    	}else{
    		return false;
    	}
    }
}
