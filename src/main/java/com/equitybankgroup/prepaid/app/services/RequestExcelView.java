package com.equitybankgroup.prepaid.app.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.equitybankgroup.prepaid.app.models.TokenRequests;

public class RequestExcelView extends AbstractExcelView {
	
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		//create a new excel sheet
		HSSFSheet excelSheet = workbook.createSheet("Transactions.xls");
		excelSheet.setDefaultColumnWidth(24);        
        
		// create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.BROWN.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
		
		HSSFRow excelHeader = excelSheet.createRow(0);
		excelHeader.createCell(0).setCellValue("METER NO");
		excelHeader.getCell(0).setCellStyle(style);
		excelHeader.createCell(1).setCellValue("TRANSACTION REF");
		excelHeader.getCell(1).setCellStyle(style);
		excelHeader.createCell(2).setCellValue("AMOUNT");
		excelHeader.getCell(2).setCellStyle(style);
		excelHeader.createCell(3).setCellValue("DATE REQUESTED");
		excelHeader.getCell(3).setCellStyle(style);
		excelHeader.createCell(4).setCellValue("PHONE NO");
		excelHeader.getCell(4).setCellStyle(style);
		excelHeader.createCell(5).setCellValue("RESPONSE CODE");
		excelHeader.getCell(5).setCellStyle(style);
		
		// get data model which is passed by the Spring container(TokenRequestController)
		@SuppressWarnings("unchecked")
		List<TokenRequests> orsTransactions = (List<TokenRequests>) model.get("Transactions");
		setExcelRows(excelSheet,orsTransactions);
		
	}
	
	public void setExcelRows(HSSFSheet excelSheet, List<TokenRequests> orsTransactions){
		int record = 1;
		for (TokenRequests tokenRequests : orsTransactions) {
			HSSFRow excelRow = excelSheet.createRow(record++);
			excelRow.createCell(0).setCellValue(tokenRequests.getMeterno());
			excelRow.createCell(1).setCellValue(tokenRequests.getTransactionRef());
			excelRow.createCell(2).setCellValue(tokenRequests.getAmount().doubleValue());
			excelRow.createCell(3).setCellValue(tokenRequests.getDateRequested());
			excelRow.createCell(4).setCellValue(tokenRequests.getPhoneNo());
			excelRow.createCell(5).setCellValue(tokenRequests.getResponseCode());			
		}
	}

}
