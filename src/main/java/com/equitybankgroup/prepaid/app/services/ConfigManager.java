package com.equitybankgroup.prepaid.app.services;

import com.equitybankgroup.prepaid.app.models.KPConfig;
import com.equitybankgroup.prepaid.app.repositories.ConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by john on 6/4/2015.
 */
@Service
public class ConfigManager {

    @Autowired
    public ConfigRepository repository;

    public KPConfig getConfig(String key){
        return repository.findByItemKey(key);
    }

}
