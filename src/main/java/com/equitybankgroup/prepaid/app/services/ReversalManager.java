package com.equitybankgroup.prepaid.app.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.equitybankgroup.prepaid.app.models.Reversals;
import com.equitybankgroup.prepaid.app.repositories.ReversalRepository;

/**
 * Created by john on 6/4/2015.
 */
@Service
public class ReversalManager {

    @Autowired
    public ReversalRepository repository;

    public Page<Reversals> getReversals(PageRequest req){
        return repository.findAll(req);
    }
    
    public Page<Reversals> getReversal(Pageable pageable){
        return repository.findAll(pageable);
    }

    public Page<Reversals> getReversalFiltered(String filter,PageRequest req){

        return repository.findByMeterNumberContaining(filter, req);
    }
    
    public Page<Reversals> getSearchedTokenReversals(String meterNumber ,Pageable pageable){
        return repository.searchTokenReversals(meterNumber, pageable);
    }
    
    public Page<Reversals> getSearchedTokenReversals(String meterNumber, String startDate, String endDate,
			Pageable pageable) {
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
		try {
			startDate = formatter2.format(formatter2.parse(formatter2.format(formatter1.parse(startDate))));
			endDate = formatter2.format(formatter2.parse(formatter2.format(formatter1.parse(endDate))));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println(startDate);
		return repository.searchTokenReversals(meterNumber, startDate, endDate, pageable);
	}

    public Reversals getReversal(String id){

        List<Reversals> revs =  repository.findByTransactionRef(id,new Sort(Sort.Direction.DESC, "dateRequested"));

        if(revs.size()>0) {
            return revs.get(0);
        }else{
            return null;
        }
    }
}
