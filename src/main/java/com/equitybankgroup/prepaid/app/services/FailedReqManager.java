package com.equitybankgroup.prepaid.app.services;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.equitybankgroup.prepaid.app.models.FailedRequests;
import com.equitybankgroup.prepaid.app.repositories.FailedRequestsRepository;

@Service
public class FailedReqManager {

	@Autowired
	FailedRequestsRepository failedRequestsRepository;

	public Page<FailedRequests> getFailedRequets(Pageable pageable) {
		return failedRequestsRepository.findAll(pageable);
	}

	public Page<FailedRequests> getFailedReqByForacid(String filter,
			Pageable pageable) {
		return failedRequestsRepository.getFailedReqByForacid(filter, pageable);
	}

	public List<FailedRequests> getFailedRequestByForacid(String filter) {
		return failedRequestsRepository.getFailedRequestByForacid(filter);
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor = RuntimeException.class)
	public boolean resetRequestStatus(List<FailedRequests> failedRequests) throws IOException{		
		for(FailedRequests failedRequest : failedRequests){
			failedRequest.setStatus("0");
			failedRequestsRepository.save(failedRequest);
		}
		return true;
	}
	
	public FailedRequests getFailedRequestById(Long id){
		return failedRequestsRepository.findOne(id);
	}
}
