package com.equitybankgroup.prepaid.app.repositories;

import com.equitybankgroup.prepaid.app.models.KPConfig;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by john on 6/4/2015.
 */
public interface ConfigRepository extends PagingAndSortingRepository<KPConfig, Long> {

    public KPConfig findByItemKey(String key);
}
