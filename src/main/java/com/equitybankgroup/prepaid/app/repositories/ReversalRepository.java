package com.equitybankgroup.prepaid.app.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.equitybankgroup.prepaid.app.models.Reversals;

/**
 * Created by john on 6/4/2015.
 */
public interface ReversalRepository extends PagingAndSortingRepository<Reversals, Long> {

    public Page<Reversals> findByMeterNumberContaining(String filter,Pageable req);

    public List<Reversals> findByTransactionRef(String id,Sort sort);
    
    @Query("select trv from Reversals trv where trv.meterNumber = :meterNumber")
	public Page<Reversals> searchTokenReversals(@Param("meterNumber") String meterNumber, Pageable pageable);
    
    @Query("select trv from Reversals trv where trv.meterNumber = :meterNumber and to_date(to_char(trv.dateRequested,'dd/mm/yyyy'), "
			+ "'dd/mm/yyyy') between to_date(:startDate, 'dd/mm/yyyy') and to_date(:endDate, 'dd/mm/yyyy')")	
	public Page<Reversals> searchTokenReversals(@Param("meterNumber") String meterNumber,
			@Param("startDate") String startDate, @Param("endDate") String endDate, Pageable pageable);
}
