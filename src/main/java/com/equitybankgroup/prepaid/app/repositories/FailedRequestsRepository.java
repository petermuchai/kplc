package com.equitybankgroup.prepaid.app.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.equitybankgroup.prepaid.app.models.FailedRequests;

public interface FailedRequestsRepository extends PagingAndSortingRepository<FailedRequests, Long>{
	
	@Query("select fr from FailedRequests fr where fr.foracid = :filter")
	public Page<FailedRequests> getFailedReqByForacid(@Param("filter") String filter, Pageable pageable);
	public List<FailedRequests> getFailedRequestByForacid(@Param("filter") String filter);

}
