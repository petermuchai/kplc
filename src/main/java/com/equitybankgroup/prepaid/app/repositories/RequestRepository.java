package com.equitybankgroup.prepaid.app.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.equitybankgroup.prepaid.app.models.TokenRequests;

/**
 * Created by john on 6/4/2015.
 */
public interface RequestRepository extends PagingAndSortingRepository<TokenRequests, Long> {

	@Query("select tr"
			+ " from TokenRequests tr "
			+ "order by tr.dateReceived desc")
	public Page<TokenRequests> findAllRequests(Pageable pageable);

	@Query("Select tr from TokenRequests tr where tr.meterno = :filter")
	public Page<TokenRequests> findByMeternoContaining(@Param("filter") String filter, Pageable pageable);

	public List<TokenRequests> findByTransactionRef(String id, Sort sort);

	@Query("select tr from TokenRequests tr where tr.phoneNo = :phoneNo and to_date(to_char(tr.dateRequested,'dd/mm/yyyy'), "
			+ "'dd/mm/yyyy') between to_date(:startDate, 'dd/mm/yyyy') and to_date(:endDate, 'dd/mm/yyyy')"
			+ "order by tr.dateReceived")
	public Page<TokenRequests> searchTokenRequests(@Param("phoneNo") String phoneNo,
			@Param("startDate") String startDate, @Param("endDate") String endDate, Pageable pageable);

	@Query("select tr from TokenRequests tr where to_date(to_char(tr.dateRequested,'dd/mm/yyyy'), "
			+ "'dd/mm/yyyy') between to_date(:startDate, 'dd/mm/yyyy') and to_date(:endDate, 'dd/mm/yyyy')"
			+ "order by tr.dateReceived desc")
	public Page<TokenRequests> searchTokenRequestsByDate(@Param("startDate") String startDate,
			@Param("endDate") String endDate, Pageable pageable);

	@Query("select count(tr.id), to_char(tr.dateRequested,'dd/mm/yyyy'), to_date(to_char(tr.dateRequested,'dd/mm/yyyy'),'dd/mm/yyyy') as dateRequested from TokenRequests tr "
			+ "where to_date(to_char(tr.dateRequested,'dd/mm/yyyy'),'dd/mm/yyyy') between"
			+ "(select to_date(to_char((max(tr.dateRequested)-10),'dd/mm/yyyy'),'dd/mm/yyyy') from TokenRequests tr)"
			+ "and" + "(select to_date(to_char(max(tr.dateRequested),'dd/mm/yyyy'),'dd/mm/yyyy') from TokenRequests tr)"
			+ "group by to_char(tr.dateRequested,'dd/mm/yyyy'),to_date(to_char(tr.dateRequested,'dd/mm/yyyy'),'dd/mm/yyyy') order by dateRequested desc")
	public Page<TokenRequests> tokenRequestCount(Pageable pageable);
	
	@Query("select tr from TokenRequests tr where to_date(to_char(tr.dateRequested,'dd/mm/yyyy'), "
			+ "'dd/mm/yyyy') between to_date(:startDate, 'dd/mm/yyyy') and to_date(:endDate, 'dd/mm/yyyy')"
			+ "order by tr.dateReceived desc")
	public List<TokenRequests> searchTokenRequestsByDateExcel(@Param("startDate") String startDate,
			@Param("endDate") String endDate);
}
