package com.equitybankgroup.prepaid.app.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.equitybankgroup.prepaid.app.models.CustMasterTranTable;


/**
 * Created by john on 6/4/2015.
 */
public interface TransactionRepository extends PagingAndSortingRepository<CustMasterTranTable, Long> {

    public Page<CustMasterTranTable> findByRefNumContaining(String filter,Pageable req);
   // public Page<CustMasterTranTable> findByRefNum(String filter,Sort sort);

    public Page<CustMasterTranTable> findByForacid(String foracid,Pageable req);

    @Query("SELECT c FROM CustMasterTranTable c WHERE c.status = '3' or c.status='4'")
    public Page<CustMasterTranTable> findFailures(Pageable req);
}
